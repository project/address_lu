<?php

namespace Drupal\address_lu\EventSubscriber;

use Drupal\address\Event\AddressEvents;
use Drupal\address\Event\AddressFormatEvent;
use Drupal\address\Event\SubdivisionsEvent;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use CommerceGuys\Addressing\AddressFormat\AddressField;
use CommerceGuys\Addressing\AddressFormat\AdministrativeAreaType;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Adds a city field and a predefined list of cities for Luxembourg.
 */
class AddressEventsSubscriber implements EventSubscriberInterface {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[AddressEvents::ADDRESS_FORMAT][] = ['onAddressFormat'];
    $events[AddressEvents::SUBDIVISIONS][] = ['onSubdivisions'];
    return $events;
  }

  /**
   * Alters the address format for Luxembourg.
   *
   * @param \Drupal\address\Event\AddressFormatEvent $event
   *   The address format event.
   */
  public function onAddressFormat(AddressFormatEvent $event) {
    $definition = $event->getDefinition();

    if (isset($definition['country_code']) && $definition['country_code'] == 'LU') {
      $definition['format'] = "%organization\n%givenName %familyName\n%administrativeArea-%locality\n%postalCode\n%addressLine1\n%addressLine2";
      $definition['subdivision_depth'] = 2;
      $definition['required_fields'][] = AddressField::ADMINISTRATIVE_AREA;
      $definition['required_fields'][] = AddressField::LOCALITY;
      $definition['administrative_area_type'] = AdministrativeAreaType::CANTON;
      $event->setDefinition($definition);
    }
  }

  /**
   * Provides the subdivisions for Luxembourg.
   *
   * @param \Drupal\address\Event\SubdivisionsEvent $event
   *   The subdivisions event.
   */
  public function onSubdivisions(SubdivisionsEvent $event) {
    $parents = $event->getParents();

    if ($parents == ['LU']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          'Capellen' => [
            'iso_code' => 'LU-CA',
            'has_children' => TRUE,
          ],
          'Clervaux' => [
            'iso_code' => 'LU-CL',
            'has_children' => TRUE,
          ],
          'Diekirch' => [
            'iso_code' => 'LU-DI',
            'has_children' => TRUE,
          ],
          'Echternach' => [
            'iso_code' => 'LU-EC',
            'has_children' => TRUE,
          ],
          'Esch-sur-Alzette' => [
            'iso_code' => 'LU-ES',
            'has_children' => TRUE,
          ],
          'Grevenmacher' => [
            'iso_code' => 'LU-GR',
            'has_children' => TRUE,
          ],
          'Luxembourg' => [
            'iso_code' => 'LU-LU',
            'has_children' => TRUE,
          ],
          'Mersch' => [
            'iso_code' => 'LU-ME',
            'has_children' => TRUE,
          ],
          'Redange' => [
            'iso_code' => 'LU-RD',
            'has_children' => TRUE,
          ],
          'Remich' => [
            'iso_code' => 'LU-RM',
            'has_children' => TRUE,
          ],
          'Vianden' => [
            'iso_code' => 'LU-VD',
            'has_children' => TRUE,
          ],
          'Wiltz' => [
            'iso_code' => 'LU-WI',
            'has_children' => TRUE,
          ],
        ],
      ];
      $event->setDefinitions($definitions);
    }

    if ($parents == ['LU', 'Capellen']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          'Dippach' => [],
          'Bettange-sur-Mess' => [],
          'Schouweiler' => [],
          'Sprinkange' => [],
          'Garnich' => [],
          'Dahlem' => [],
          'Hivange' => [],
          'Kahler' => [],
          'Eischen' => [],
          'Greisch' => [],
          'Hobscheid' => [],
          'Roodt/Eisch' => [],
          'Septfontaines' => [],
          'Simmerfarm' => [],
          'Simmerschmelz' => [],
          'Bascharage' => [],
          'Clemency' => [],
          'Fingig' => [],
          'Hautcharage' => [],
          'Linger' => [],
          'Kehlen' => [],
          'Dondelange' => [],
          'Keispelt' => [],
          'Meispelt' => [],
          'Nospelt' => [],
          'Olm' => [],
          'Koerich' => [],
          'Goeblange' => [],
          'Goetzingen' => [],
          'Windhof (Koerich)' => [],
          'Bridel' => [],
          'Kopstal' => [],
          'Mamer' => [],
          'Capellen' => [],
          'Holzem' => [],
          'Steinfort' => [],
          'Grass' => [],
          'Hagen' => [],
          'Kleinbettingen' => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }

    if ($parents == ['LU', 'Clervaux']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          'Clervaux' => [],
          'Drauffelt' => [],
          'Eselborn' => [],
          'Fischbach' => [],
          'Fossenhof' => [],
          'Grindhausen' => [],
          'Heinerscheid' => [],
          'Hupperdange' => [],
          'Kaesfurt' => [],
          'Kalborn' => [],
          'Kirelshof' => [],
          'Lausdorn' => [],
          'Lieler' => [],
          'Marnach' => [],
          'Mecher' => [],
          'Munshausen' => [],
          'Reuler' => [],
          'Roder' => [],
          'Siebenaler' => [],
          'Tintesmühle' => [],
          'Urspelt' => [],
          'Weicherdange' => [],
          'Wirtgensmühle' => [],
          'Bockholtz' => [],
          'Consthum' => [],
          'Dorscheid' => [],
          'Eisenbach' => [],
          'Holzthum' => [],
          'Hoscheid' => [],
          'Hoscheid-Dickt' => [],
          'Hosingen' => [],
          'Neidhausen' => [],
          'Rodershausen' => [],
          'Unterschlinder' => [],
          'Wahlhausen' => [],
          'Troisvierges' => [],
          'Wilwerdange' => [],
          'Huldange' => [],
          'Hautbellain' => [],
          'Goedange' => [],
          'Drinklange' => [],
          'Biwisch' => [],
          'Basbellain' => [],
          'Weiswampach' => [],
          'Beiler' => [],
          'Binsfeld' => [],
          'Breidfeld' => [],
          'Holler' => [],
          'Kleemühle' => [],
          'Leithum' => [],
          'Maulusmühle' => [],
          'Wemperhardt' => [],
          'Wincrange' => [],
          'Allerborn' => [],
          'Asselborn' => [],
          'Boevange' => [],
          'Boxhorn' => [],
          'Brachtenbach' => [],
          'Cinqfontaines' => [],
          'Crendal' => [],
          'Deiffelt' => [],
          'Derenbach' => [],
          'Doennange' => [],
          'Hachiville' => [],
          'Hamiville' => [],
          'Hinterhassel' => [],
          'Hoffelt' => [],
          'Lentzweiler' => [],
          'Lullange' => [],
          'Maulusmuehle' => [],
          'Niederwampach' => [],
          'Oberwampach' => [],
          'Rumlange' => [],
          'Sassel' => [],
          'Schimpach' => [],
          'Stockem' => [],
          'Troine' => [],
          'Troine-Route' => [],
          'Weiler' => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }

    if ($parents == ['LU', 'Diekirch']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          'Bettendorf' => [],
          'Gilsdorf' => [],
          'Moestroff' => [],
          'Bourscheid' => [],
          'Bourscheid-Moulin' => [],
          'Bourscheid-Plage' => [],
          'Closdelt' => [],
          'Dirbach' => [],
          'Enteschbach' => [],
          'Flebour' => [],
          'Friedbusch' => [],
          'Goebelsmühle' => [],
          'Kehmen' => [],
          'Lipperscheid' => [],
          'Michelau' => [],
          'Scheidel' => [],
          'Schlindermanderscheid' => [],
          'Welscheid' => [],
          'Diekirch' => [],
          'Erpeldange-sur-Sûre' => [],
          'Burden' => [],
          'Ingeldorf' => [],
          'Ettelbruck' => [],
          'Grentzingen' => [],
          'Warken' => [],
          'Niederfeulen' => [],
          'Oberfeulen' => [],
          'Mertzig' => [],
          'Reisdorf' => [],
          'Bigelbach' => [],
          'Hoesdorf' => [],
          'Wallendorf-Pont' => [],
          'Schieren' => [],
          'Birtrange' => [],
          'Colmar-Pont' => [],
          'Eppeldorf' => [],
          'Ermsdorf' => [],
          'Folkendange' => [],
          'Keiwelbach' => [],
          'Medernach' => [],
          'Savelborn' => [],
          'Stegen' => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }

    if ($parents == ['LU', 'Echternach']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          'Beaufort' => [],
          'Dillingen' => [],
          'Grundhof' => [],
          'Bech' => [],
          'Altrier' => [],
          'Blumenthal' => [],
          'Geyershof' => [],
          'Graulinster' => [],
          'Hemstal' => [],
          'Hersberg' => [],
          'Kobenbour' => [],
          'Rippig' => [],
          'Zittig' => [],
          'Berdorf' => [],
          'Bollendorf-Pont' => [],
          'Grundhof (Berdorf)' => [],
          'Kalkesbach (Berdorf)' => [],
          'Weilerbach' => [],
          'Consdorf' => [],
          'Breidweiler' => [],
          'Colbette' => [],
          'Kalkesbach (Consdorf)' => [],
          'Marscherwald' => [],
          'Scheidgen' => [],
          'Wolper' => [],
          'Echternach' => [],
          'Born' => [],
          'Boursdorf' => [],
          'Dickweiler' => [],
          'Girst' => [],
          'Girsterklaus' => [],
          'Givenich' => [],
          'Herborn' => [],
          'Hinkel' => [],
          'Lilien' => [],
          'Moersdorf' => [],
          'Mompach' => [],
          'Osweiler' => [],
          'Rosport' => [],
          'Steinheim' => [],
          'Waldbillig' => [],
          'Christnach' => [],
          'Freckeisen' => [],
          'Haller' => [],
          'Mullerthal' => [],
          'Savelborn (Waldbillig)' => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }

    if ($parents == ['LU', 'Esch-sur-Alzette']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          'Esch-sur-Alzette' => [],
          'Bettembourg' => [],
          'Abweiler' => [],
          'Fennange' => [],
          'Huncherange' => [],
          'Differdange' => [],
          'Lasauvage' => [],
          'Niederkorn' => [],
          'Oberkorn' => [],
          'Dudelange' => [],
          'Frisange' => [],
          'Aspelt' => [],
          'Hellange' => [],
          'Kayl' => [],
          'Tétange' => [],
          'Leudelange' => [],
          'Mondercange' => [],
          'Bergem' => [],
          'Foetz' => [],
          'Pontpierre' => [],
          'Pétange' => [],
          'Lamadelaine' => [],
          'Rodange' => [],
          'Reckange-sur-Mess' => [],
          'Ehlange' => [],
          'Limpach' => [],
          'Pissange' => [],
          'Roedgen' => [],
          'Wickrange' => [],
          'Roeser' => [],
          'Berchem' => [],
          'Bivange' => [],
          'Crauthem' => [],
          'Kockelscheuer' => [],
          'Livange' => [],
          'Peppange' => [],
          'Rumelange' => [],
          'Sanem' => [],
          'Belvaux' => [],
          'Ehlerange' => [],
          'Soleuvre' => [],
          'Schifflange' => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }

    if ($parents == ['LU', 'Grevenmacher']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          'Grevenmacher' => [],
          'Betzdorf' => [],
          'Berg' => [],
          'Mensdorf' => [],
          'Olingen' => [],
          'Roodt-sur-Syre' => [],
          'Biwer' => [],
          'Biwerbach' => [],
          'Boudler' => [],
          'Boudlerbach' => [],
          'Breinert' => [],
          'Brouch' => [],
          'Hagelsdorf' => [],
          'Wecker' => [],
          'Wecker-Gare' => [],
          'Weydig' => [],
          'Flaxweiler' => [],
          'Beyren' => [],
          'Gostingen' => [],
          'Niederdonven' => [],
          'Oberdonven' => [],
          'Junglinster' => [],
          'Altlinster' => [],
          'Beidweiler' => [],
          'Blumenthal' => [],
          'Bourglinster' => [],
          'Eisenborn' => [],
          'Eschweiler' => [],
          'Godbrange' => [],
          'Gonderange' => [],
          'Graulinster' => [],
          'Imbringen' => [],
          'Rodenbourg' => [],
          'Manternach' => [],
          'Berbourg' => [],
          'Lellig' => [],
          'Münschecker' => [],
          'Mertert' => [],
          'Wasserbillig' => [],
          'Wormeldange' => [],
          'Ahn' => [],
          'Dreiborn' => [],
          'Ehnen' => [],
          'Kapenacker' => [],
          'Machtum' => [],
          'Wormeldange-Haut' => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }

    if ($parents == ['LU', 'Luxembourg']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          'Luxembourg' => [],
          'Bertrange' => [],
          'Contern' => [],
          'Medingen' => [],
          'Moutfort' => [],
          'Munsbach' => [],
          'Oetrange' => [],
          'Hesperange' => [],
          'Alzingen' => [],
          'Fentange' => [],
          'Howald' => [],
          'Itzig' => [],
          'Niederanven' => [],
          'Ernster' => [],
          'Hostert' => [],
          'Oberanven' => [],
          'Rameldange' => [],
          'Senningen' => [],
          'Senningerberg' => [],
          'Waldhof' => [],
          'Sandweiler' => [],
          'Findel' => [],
          'Schuttrange' => [],
          'Neuhaeusgen' => [],
          'Schrassig' => [],
          'Uebersyren' => [],
          'Steinsel' => [],
          'Heisdorf' => [],
          'Mullendorf' => [],
          'Strassen' => [],
          'Walferdange' => [],
          'Helmsange' => [],
          'Bereldange' => [],
          'Weiler-la-Tour' => [],
          'Hassel' => [],
          'Syren' => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }

    if ($parents == ['LU', 'Mersch']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          'Mersch' => [],
          'Bissen' => [],
          'Roost (Bissen)' => [],
          'Colmar-Berg' => [],
          'Welsdorf' => [],
          'Fischbach' => [],
          'Angelsberg' => [],
          'Koedange' => [],
          'Schiltzberg' => [],
          'Schoos' => [],
          'Stuppicht' => [],
          'Weyer' => [],
          'Heffingen' => [],
          'Reuland' => [],
          'Ansembourg' => [],
          'Bill' => [],
          'Boevange-sur-Attert' => [],
          'Bour' => [],
          'Brouch' => [],
          'Buschdorf' => [],
          'Claushof' => [],
          'Finsterthal' => [],
          'Grevenknapp' => [],
          'Hollenfels' => [],
          'Marienthal' => [],
          'Kuelbecherhaff' => [],
          'Openthalt' => [],
          'Tuntange' => [],
          'Larochette' => [],
          'Ernzen' => [],
          'Meysembourg' => [],
          'Lintgen' => [],
          'Prettingen' => [],
          'Lorentzweiler' => [],
          'Gosseldange' => [],
          'Klingelscheuer' => [],
          'Blaschette' => [],
          'Asselscheuer' => [],
          'Helmdange' => [],
          'Hunsdorf' => [],
          'Beringen' => [],
          'Essingen' => [],
          'Moesdorf' => [],
          'Pettingen' => [],
          'Reckange' => [],
          'Rollingen' => [],
          'Schoenfels' => [],
          'Nommern' => [],
          'Cruchten' => [],
          'Niederglabach' => [],
          'Oberglabach' => [],
          'Schrondweiler' => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }

    if ($parents == ['LU', 'Redange']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          'Beckerich' => [],
          'Elvange' => [],
          'Hovelange' => [],
          'Huttange' => [],
          'Levelange' => [],
          'Noerdange' => [],
          'Oberpallen' => [],
          'Schweich' => [],
          'Ell' => [],
          'Colpach-Bas' => [],
          'Colpach-Haut' => [],
          'Petit-Nobressart' => [],
          'Roodt (Ell)' => [],
          'Grosbous' => [],
          'Dellen' => [],
          'Grevels (Grosbous)' => [],
          'Lehrhof' => [],
          'Bettborn' => [],
          'Platen' => [],
          'Pratz' => [],
          'Reimberg' => [],
          'Rambrouch' => [],
          'Arsdorf' => [],
          'Bigonville' => [],
          'Bigonville-Poteau' => [],
          'Bilsdorf' => [],
          'Eschette' => [],
          'Folschette' => [],
          'Haut-Martelange' => [],
          'Holtz' => [],
          'Hostert' => [],
          'Flatzbour' => [],
          'Koetschette' => [],
          'Perlé' => [],
          'Riesenhof' => [],
          'Rombach-Martelange' => [],
          'Wolwelange' => [],
          'Eltz' => [],
          'Lannen' => [],
          'Nagem' => [],
          'Niederpallen' => [],
          'Ospern' => [],
          'Redange/Attert' => [],
          'Reichlange' => [],
          'Saeul' => [],
          'Calmus' => [],
          'Ehner' => [],
          'Kapweiler' => [],
          'Schwebach' => [],
          'Useldange' => [],
          'Everlange' => [],
          'Rippweiler' => [],
          'Schandel' => [],
          'Vichten' => [],
          'Rindschleiden' => [],
          'Wahl' => [],
          'Michelbouch' => [],
          'Kuborn' => [],
          'Brattert' => [],
          'Buschrodt' => [],
          'Grevels' => [],
          'Heispelt' => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }

    if ($parents == ['LU', 'Remich']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          'Remich' => [],
          'Bous' => [],
          'Assel' => [],
          'Erpeldange' => [],
          'Rolling' => [],
          'Dalheim' => [],
          'Filsdorf' => [],
          'Welfrange' => [],
          'Lenningen' => [],
          'Canach' => [],
          'Mondorf-les-Bains' => [],
          'Altwies' => [],
          'Ellange' => [],
          'Schengen' => [],
          'Bech-Kleinmacher' => [],
          'Burmerange' => [],
          'Elvange' => [],
          'Emerange' => [],
          'Remerschen' => [],
          'Schwebsingen' => [],
          'Wellenstein' => [],
          'Wintrange' => [],
          'Stadtbredimus' => [],
          'Greiveldange' => [],
          'Huettermuehle' => [],
          'Waldbredimus' => [],
          'Ersange' => [],
          'Roedt' => [],
          'Trintange' => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }

    if ($parents == ['LU', 'Vianden']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          'Vianden' => [],
          'Putscheid' => [],
          'Bivels' => [],
          'Gralingen' => [],
          'Merscheid' => [],
          'Nachtmanderscheid' => [],
          'Stolzembourg' => [],
          'Weiler' => [],
          'Tandel' => [],
          'Bastendorf' => [],
          'Bettel' => [],
          'Brandenbourg' => [],
          'Fouhren' => [],
          'Hoscheidterhof' => [],
          'Këppenhaff' => [],
          'Landscheid' => [],
          'Longsdorf' => [],
          'Seltz' => [],
          'Walsdorf' => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }

    if ($parents == ['LU', 'Wiltz']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          'Wiltz' => [],
          'Erpeldange' => [],
          'Eschweiler' => [],
          'Knaphoscheid' => [],
          'Roullingen' => [],
          'Selscheid' => [],
          'Weidingen' => [],
          'Boulaide' => [],
          'Baschleiden' => [],
          'Surré' => [],
          'Esch-sur-Sûre' => [],
          'Bonnal' => [],
          'Dirbach' => [],
          'Eschdorf' => [],
          'Heiderscheid' => [],
          'Heiderscheidergrund' => [],
          'Hierheck' => [],
          'Insenborn' => [],
          'Lultzhausen' => [],
          'Merscheid' => [],
          'Neunhausen' => [],
          'Ringel' => [],
          'Tadler' => [],
          'Goesdorf' => [],
          'Bockholtz (Goesdorf)' => [],
          'Buederscheid' => [],
          'Dahl' => [],
          'Dirbach (Goesdorf)' => [],
          'Goebelsmuehle' => [],
          'Masseler' => [],
          'Nocher' => [],
          'Nocher-Route' => [],
          'Alscheid' => [],
          'Enscherange' => [],
          'Kautenbach' => [],
          'Lellingen' => [],
          'Merkholtz' => [],
          'Pintsch' => [],
          'Wilwerwiltz' => [],
          'Bavigne' => [],
          'Harlange' => [],
          'Kaundorf' => [],
          'Liefrange' => [],
          'Mecher' => [],
          'Nothum' => [],
          'Tarchamps' => [],
          'Watrange' => [],
          'Winseler' => [],
          'Berlé' => [],
          'Doncols' => [],
          'Gruemelscheid' => [],
          'Noertrange' => [],
          'Pommerloch' => [],
          'Schleif' => [],
          'Sonlez' => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
  }

}
